import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  private getHomeApi = "https://127.0.0.1/artemis/api/resource/v1/person/personList";
  constructor(
    private http: HttpClient,
  ) { }


  getHome(): Observable<any> {
    return this.http.get<any>(`${this.getHomeApi}`);
  }

}
