import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { HomeService } from '../service/home.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  displayedColumns = ['no','personName', 'email', 'phoneNo'];
  dataSource = new MatTableDataSource([]);
  public isLoading: boolean = false;
  public event!: PageEvent;
  public pageIndex = 0;
  public pageSizeOptions: Array<any> = [];
  public pageSize!: number;
  public totalCount: any;
  public state: string = '';
  public technology: string = '';
 
  constructor(
    private homeService: HomeService,

  ) {
    this.pageSizeOptions = [15, 20, 25, 30];
    this.pageSize = this.pageSizeOptions[0];
  }

  ngOnInit(): void {
        this.getEmployeeList(this.event);
      }



  getEmployeeList(event: PageEvent, type?: string) {
    type === 'Filter' ? this.paginator.firstPage() : '';
    this.isLoading = true;
    if (event) {
      this.pageIndex = event.pageIndex;
      this.pageSize = event.pageSize;
    }
    const params = {
      pageNo: type === 'Pagination' ? this.pageIndex + 1 : 1,
      limit: this.pageSize,
      technology: this.technology.trim().toLowerCase(),
      status: this.state,
    };
    this.homeService.getHome().subscribe({
      next: (res: any) => {
        this.isLoading = false;
        this.dataSource = new MatTableDataSource(res.data);
        this.totalCount = res.count;
      },
      error: () => {
      },
    });
  }


}